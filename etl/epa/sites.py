import numpy as np
import os
import util


class DataArray():

    def __init__(self, size, code, name):
        self.code = code
        self.name = name
        self.data = np.zeros(size, dtype=np.float)
        self.quality = np.zeros(size, dtype=np.float)
        self.unit = np.zeros(size, dtype=object)
        self.qualifier = np.zeros(size, dtype=object)
        self.uncertainty = np.zeros(size, dtype=object)
        self.MDL = np.zeros(size, dtype=object)

class Site():

    def __init__(self, year, state_code, county_code, site_num, lat, lon):
        self._tabla = util.generate_dates(year)
        self.state_code = state_code
        self.county_code = county_code
        self.site_num = site_num
        self.lat = lat
        self.lon = lon
        self.year = year
        self.codes = {}

    def add(self, code, name, date, time, value, unit, qualifier, uncertainty, MDL):
        date_key = date + ' '+time
        if code not in self.codes:
            self.codes[code] = DataArray(len(self._tabla), code, name)
        if date_key in self._tabla:
            ind = self._tabla[date_key]
            self.codes[code].data[ind] = value
            self.codes[code].quality[ind] = 1
            self.codes[code].unit[ind] = unit
            self.codes[code].qualifier[ind] = qualifier
            self.codes[code].uncertainty[ind] = uncertainty
            self.codes[code].MDL[ind] = MDL
        else:
            print(date_key, 'missing')

    def save(self, base):
        fn = f"{self.state_code:02d}-{self.county_code:03d}-{self.site_num:04d}"
        site_path = os.path.join(base, fn)
        os.makedirs(site_path, exist_ok = True)
        year_path = os.path.join(site_path, f"{self.year}")
        os.makedirs(year_path, exist_ok = True)
        for code in self.codes:
            filename = os.path.join(year_path, f"{code}.npz")
            print(filename)
            np.savez(filename, 
                year = self.year, 
                state_code = self.state_code,
                county_code = self.county_code, 
                site_num = self.site_num, 
                lat = self.lat, 
                lon = self.lon, 
                data = self.codes[code].data,
                quality = self.codes[code].quality,
                unit = self.codes[code].unit,
                qualifier = self.codes[code].qualifier,
                uncertainty = self.codes[code].uncertainty,
                MDL = self.codes[code].MDL
            )


class Crawler():

    def __init__(self, year, group):
        self.group = group
        self.year = year
        self.sites = {}

    def add(self, state_code, county_code, site_num, lat, lon, code, name, date, time, value, unit, qualifier, uncertainty, MDL):
        site_key = f"{state_code}-{county_code}-{site_num}"
        if site_key not in self.sites:
            site = Site(self.year, state_code, county_code, site_num, lat, lon)
            self.sites[site_key] = site
        site = self.sites[site_key]
        site.add(code, name, date, time, value, unit, qualifier, uncertainty, MDL)

    def save(self, base):
        for site_key in self.sites:
            site = self.sites[site_key]
            site.save(base)

if __name__ == "__main__":
    print('--- sites ----')
