Pre preuzimanja za epa servera
(base) djordje@djordjepc4:~/data/git/atlas-deliverable$ df -h
Filesystem      Size  Used Avail Use% Mounted on
udev             16G     0   16G   0% /dev
tmpfs           3,2G  2,5M  3,2G   1% /run
/dev/nvme0n1p2  468G  270G  175G  61% /
tmpfs            16G     0   16G   0% /dev/shm
tmpfs           5,0M  4,0K  5,0M   1% /run/lock
tmpfs            16G     0   16G   0% /sys/fs/cgroup
/dev/loop0       56M   56M     0 100% /snap/core18/2284
/dev/loop1      128K  128K     0 100% /snap/bare/5
/dev/loop2      219M  219M     0 100% /snap/gnome-3-34-1804/66
/dev/loop4      219M  219M     0 100% /snap/gnome-3-34-1804/77
/dev/loop5       55M   55M     0 100% /snap/snap-store/558
/dev/loop6       66M   66M     0 100% /snap/gtk-common-themes/1519
/dev/loop8       52M   52M     0 100% /snap/snap-store/518
/dev/nvme0n1p1  511M  5,3M  506M   2% /boot/efi
/dev/loop9      249M  249M     0 100% /snap/gnome-3-38-2004/99
/dev/loop10      65M   65M     0 100% /snap/gtk-common-themes/1514
tmpfs           3,2G   16K  3,2G   1% /run/user/125
/dev/loop13      44M   44M     0 100% /snap/snapd/14978
tmpfs           3,2G  4,0K  3,2G   1% /run/user/1001
/dev/loop3       62M   62M     0 100% /snap/core20/1361
/dev/loop7       62M   62M     0 100% /snap/core20/1376
/dev/loop12      44M   44M     0 100% /snap/snapd/15177
/dev/loop14      56M   56M     0 100% /snap/core18/2344
tmpfs           3,2G  4,0K  3,2G   1% /run/user/1003


State Code
County Code
Site Num
Parameter Code
POC
Latitude
Longitude
Datum
Parameter Name
Date Local
Time Local
Date GMT
Time GMT
Sample Measurement
Units of Measure
MDL
Uncertainty
Qualifier
Method Type
Method Code
Method Name
State Name
County Name
Date of Last Change

0:01:24.860200 /home/atlas/data/atlas_data/tmp/hourly_44201_2021.csv (5988236, 24)