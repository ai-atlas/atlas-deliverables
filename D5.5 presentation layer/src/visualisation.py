import numpy as np

from ipyleaflet import CircleMarker
from ipyleaflet import Map, basemaps
from ipyleaflet import Popup
from ipywidgets import HTML
from ipyleaflet import MarkerCluster
from ipyleaflet import LayerGroup
from ipyleaflet import ScaleControl
from ipyleaflet import LegendControl
from ipyleaflet import LayersControl
from ipyleaflet import AwesomeIcon
from ipyleaflet import Marker
import branca.colormap as cm

def show_map(values, colors):
    colormap = cm.LinearColormap(colors=colors, vmin=0, vmax=6)

    land_use_map = {
        'COMMERCIAL': colormap(0),
        'AGRICULTURAL': colormap(1),
        'RESIDENTIAL': colormap(2),
        'FOREST': colormap(3),
        'INDUSTRIAL': colormap(4),
        'MOBILE': colormap(5),
         6 :colormap(6),
    }

    # location_setting_map = {
    #     'URBAN AND CENTER CITY': colormap(0),
    #     'RURAL': colormap(1),
    #     'SUBURBAN': colormap(2),
    #     np.nan :colormap(3),
    # }

    markers = []
    for _, id, lon, lat, elevation, land_use, location_settings in values:
        color = '#ff0000'
        r = 5
        if land_use in land_use_map:
            color = land_use_map[land_use]
        circle = CircleMarker(location=(lat, lon), color=color, fill_color=color, weight=3, radius=5)
        message = HTML(value=f"{id}, {land_use}")
        circle.popup = message
        markers.append(circle)

    m = Map(zoom=4, center=(40.79,-73.98))
    # m.add_layer(basemaps.OpenStreetMap.BlackAndWhite)
    m.add_layer(basemaps.CartoDB.Positron)

    layer_group = LayerGroup(layers=markers, name="Stanice")
    m.add_layer(layer_group)

    marker_cluster = MarkerCluster(markers=markers, name="Grupe")
    m.add_layer(marker_cluster)
    m.add_control(ScaleControl(position='bottomleft'))
    legend = LegendControl(land_use_map, name="Legend", position="topright")
    control = LayersControl(position='topleft', expanded=True)
    m.add_control(control)
    m.add_control(legend)

    m.layout.height="700px"
    return m