FROM ubuntu:20.04

RUN DEBIAN_FRONTEND=noninteractive apt-get update
# RUN DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade
RUN DEBIAN_FRONTEND=noninteractive apt-get -yq install nginx python3-pip

RUN apt-get -y install libglib2.0-0
RUN apt-get -y install libsm6 \
     libxrender-dev \
     libxext6
RUN apt-get install ffmpeg libsm6 libxext6  -y

RUN apt-get install -y python3-opencv
RUN pip install opencv-python

RUN echo 'alias ..="cd .."' >> ~/.bashrc
RUN echo 'alias p="python3"' >> ~/.bashrc

COPY requirements.txt /requirements.txt
RUN pip3 install -r /requirements.txt

WORKDIR /home