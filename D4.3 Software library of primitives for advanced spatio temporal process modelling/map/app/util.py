from __future__ import print_function
import sys
from functools import wraps
import datetime
from datetime import timedelta
from json import JSONEncoder
from difflib import SequenceMatcher
from app.db import mongo_db
from datetime import timedelta
from bson.objectid import ObjectId
import arrow


# from pympler import tracker
#
#
def profiler(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # tr = tracker.SummaryTracker()
        start = datetime.datetime.now()
        ret_val = f(*args, **kwargs)
        # tr.print_diff()
        end = datetime.datetime.now()
        log(str(f)+': '+str(end-start))
        return ret_val
    return decorated_function


class cached(object):
    def __init__(self, *args, **kwargs):
        self.cached_function_responses = {}
        self.default_max_age = kwargs.get("default_cache_max_age", timedelta(seconds=30))

    def __call__(self, func):
        def inner(*args, **kwargs):
            max_age = kwargs.get('max_age', self.default_max_age)
            aa = ''
            for arg in args:
                aa += '{} '.format(arg)
            key = "{} {}".format(func, aa)
            if not max_age or key not in self.cached_function_responses or (datetime.datetime.now() - self.cached_function_responses[key]['fetch_time'] > max_age):
                if 'max_age' in kwargs: del kwargs['max_age']
                res = func(*args, **kwargs)
                self.cached_function_responses[key] = {'data': res, 'fetch_time': datetime.datetime.now()}
            return self.cached_function_responses[key]['data']
        return inner

class DateTimeEncoder(JSONEncoder):

    def default(self, obj):
        if isinstance(obj, DBObject):
            obj = obj.__dict__.copy()
            if '__fields__' in obj:
                del obj['__fields__']

        if isinstance(obj, datetime.datetime):
            encoded_object = obj.isoformat()
        else:
            encoded_object = JSONEncoder.default(self, obj)
        return encoded_object


class DBObject(object):
    def __init__(self):
        self.__fields__ = []

    def __setattr__(self, attr, value):
        if '__fields__' in self.__dict__:
            if attr not in self.__dict__ and attr != '__fields__':
                self.__fields__.append(attr)
        super.__setattr__(self, attr, value)

    def toDbObject(self, projection=None):
        dict = {}
        for key in self.__fields__:
            value = self.__dict__[key]
            if isinstance(value, DBObject):
                dict[key] = value.toDbObject()
            elif isinstance(value, ObjectId):
                dict[key] = str(value)
            elif isinstance(value, type([])):
                ll = []
                for el in value:
                    if isinstance(el, DBObject):
                        ll.append(el.toDbObject())
                    else:
                        ll.append(el)
                dict[key] = ll
            elif isinstance(value, type({})):
                ll = {}
                for kkey in value:
                    el = value[kkey]
                    if isinstance(el, DBObject):
                        ll[kkey] = el.toDbObject()
                    else:
                        ll[kkey] = el
                dict[key] = ll
            else:
                dict[key] = value
        if projection is not None:
            ddict = {}
            for k in dict:
                if k in projection:
                    ddict[k] = dict[k]
            dict = ddict
        return dict

    def toJSON(self, projection=None):
        # dict = self.__dict__.copy()
        # if '__fields__' in dict:
        #     del dict['__fields__']
        dict = {}
        for key in self.__fields__:
            value = self.__dict__[key]
            if isinstance(value, DBObject):
                dict[key] = value.toJSON()
            elif isinstance(value, type([])):
                ll = []
                for i, el in enumerate(value):
                    if isinstance(el, DBObject):
                        ll.append(el.toJSON())
                    else:
                        ll.append(el)
                dict[key] = ll
            elif isinstance(value, type({})):
                ll = {}
                for kkey in value:
                    el = value[kkey]
                    if isinstance(el, DBObject):
                        ll[kkey] = el.toJSON()
                    elif isinstance(el, datetime.datetime):
                        ll[kkey] = el.isoformat()
                    elif isinstance(el, type({})):
                        ll[kkey] = convert_dict_to_JSON(el)
                    else:
                        ll[kkey] = el
                dict[key] = ll
            elif isinstance(value, datetime.datetime):
                dict[key] = value.isoformat()
            else:
                dict[key] = value

        if projection is not None:
            ddict = {}
            for k in dict:
                if k in projection:
                    ddict[k] = dict[k]
            dict = ddict
        return dict

    def fromJSON(self, dict):
        for k in dict:
            if k == 'choices':
                self.bets = []
                for el in dict[k]:
                    self.bets.append(el)
            else:
                self.__setattr__(k, dict[k])


def dt_parse(t):
    #018-04-20T15:55:35+02:00
    print('---------> dt_parse  {}'+t)
    ret = datetime.datetime.strptime(t[0:16], '%Y-%m-%dT%H:%M')
    if t[18] == '+':
        ret -= timedelta(hours=int(t[19:22]), minutes=int(t[23:]))
    elif t[18] == '-':
        ret += timedelta(hours=int(t[19:22]), minutes=int(t[23:]))
    return ret

def convert_dict_to_JSON(dict):
    ret_val = {}
    for key in dict:
        value = dict[key]
        if isinstance(value, DBObject):
            dict[key] = value.toJSON()
        elif isinstance(value, datetime.datetime):
            ret_val[key] = value.isoformat()
        elif isinstance(value, ObjectId):
            ret_val[key] = str(value)
        elif isinstance(value, type({})):
            ret_val[key] = convert_dict_to_JSON(value)
        elif isinstance(value, type([])):
            ll = []
            for el in value:
                if isinstance(el, DBObject):
                    ll.append(el.toJSON())
                elif isinstance(el, datetime.datetime):
                    ll.append(el.isoformat())
                elif isinstance(el, type({})):
                    ll.append(convert_dict_to_JSON(el))
                else:
                    ll.append(el)
            ret_val[key] = ll
        else:
            ret_val[key] = value
    return ret_val


def convert_to_isodates(obj):
    """Avoid problem with datetime serialization"""
    if type(obj) == type([]):
        for el in obj:
            # print(el)
            for k in el:
                eel = el[k]
                if isinstance(eel, datetime.datetime):
                    el[k] = eel.isoformat()
    else:
        for k in obj:
            eel = obj[k]
            if isinstance(eel, datetime.datetime):
                obj[k] = eel.isoformat()
    return obj


def from_isodate(obj):
    """ 2008-09-03T20:56:35.450686Z """
    # print("--------------------{}".format(obj))
    if(obj is None):
        return None
    else:
        el = arrow.get(obj).datetime
        print(el)
        return el
        # # 2018-04-20T15:20:00+02:00
        # if obj[-1]=='Z':
        #     try:
        #         return datetime.datetime.strptime(obj, "%Y-%m-%dT%H:%M:%S.%fZ")
        #     except Exception as cc:
        #         log(cc)
        # if obj[-1]=='z':
        #     try:
        #         return datetime.datetime.strptime(obj, "%Y-%m-%dT%H:%M:%S%z")    
        #     except Exception as cc:
        #         log(cc)
        


def log(message):
    print(message, file=sys.stderr)

def requests_log(url, exec_time):
    collection = mongo_db.requests_log
    if exec_time is None:
        collection.update({'url': url},
                          {'$inc': {'count': 1}},
                          upsert=True)
    else:
        collection.update({'url': url},
                          {'$set': {'exec_time': exec_time}},
                          upsert=True)

def get_el(obj, key, default):
    if key in obj:
        return obj[key]
    else:
        return default

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def to_int(el, default):
    try:
        return int(el)
    except:
        return default


def to_float(el, default):
    try:
        return float(el)
    except:
        return default


def to_latitude(s_latitude, s_lat):
    try:
        latDD = float(s_latitude[0:2])
        latMM = float(s_latitude[2:4])
        latSS = float(s_latitude[5:9])
        lat = latDD + (latMM + latSS/10000.0)/60.0
        if(s_lat == "S"):
            lat = -1*lat
        return lat
    except Exception as e:
        return 0


def to_longitude(s_longitude, s_lon):
    try:
        lonDDD = float(s_longitude[0:3])
        lonMM = float(s_longitude[3:5])
        lonSS = float(s_longitude[6:10])
        lon = lonDDD + (lonMM + lonSS/10000.0)/60.0
        if(s_lon == "W"):
            lon = -1*lon
        return lon
    except Exception as e:
        return 0


from pyproj import Proj, transform
import math
from pyproj import Geod
wgs84_geod = Geod(ellps='WGS84')


inProj = Proj(init='epsg:4326')
outProj = Proj(init='epsg:32634')


"""
The Ramer-Douglas-Peucker algorithm roughly ported from the pseudo-code provided
by http://en.wikipedia.org/wiki/Ramer-Douglas-Peucker_algorithm
"""

from math import sqrt


def distance(a, b):
    return sqrt((a['utm'][0] - b['utm'][0]) ** 2 + (a['utm'][1] - b['utm'][1]) ** 2)


def point_line_distance(point, start, end):
    if (start['utm'] == end['utm']):
        return distance(point, start)
    else:
        n = abs(
            (end['utm'][0] - start['utm'][0]) * (start['utm'][1] - point['utm'][1]) -
            (start['utm'][0] - point['utm'][0]) *
            (end['utm'][1] - start['utm'][1])
        )
        d = sqrt(
            (end['utm'][0] - start['utm'][0]) ** 2 +
            (end['utm'][1] - start['utm'][1]) ** 2
        )
        return n / d


def rdp(points, epsilon):
    """
    Reduces a series of points to a simplified version that loses detail, but
    maintains the general shape of the series.
    """
    dmax = 0.0
    index = 0
    for i in range(1, len(points) - 1):
        d = point_line_distance(points[i], points[0], points[-1])
        if d > dmax:
            index = i
            dmax = d
    if dmax >= epsilon:
        results = rdp(points[:index+1], epsilon)[:-1] + \
            rdp(points[index:], epsilon)
    else:
        # odrediti prosecnu brzinu, max brzinu i prosecan pritisak
        max_speed = 0
        s = 0
        n = 0
        for el in points:
            max_speed = max([el['speed'], max_speed])
            if 'extd' in el:
                if el['extd'] != None and len(el['extd'])>0:
                    for elv in el['extd'][0]['values']:
                        if elv['name'] == 'F':
                            try:
                                s += float(elv['value'])
                                n += 1
                            except:
                                pass
        points[0]['max_speed'] = max_speed
        points[-1]['max_speed'] = max_speed
        if n>0:
            points[0]['avg_F'] = s/n
            points[-1]['avg_F'] = s/n
        results = [points[0], points[-1]]
    return results
