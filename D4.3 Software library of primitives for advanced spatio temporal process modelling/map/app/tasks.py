#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os

from flask_restful import reqparse, abort, Resource
from flask import request
from flask import Response
from bson.objectid import ObjectId
from zipfile import ZipFile

import json

from app.auth.util import login_required

from app.db import mongo_db
from app.util import DBObject
import itertools
import json
from app.util import convert_dict_to_JSON
from bson.objectid import ObjectId
import xlwt
# import StringIO
import traceback

from lxml import etree
# from pykml import parser
import pyproj
import shapely
import shapely.ops as ops
from shapely.geometry.polygon import Polygon
from functools import partial


def tasks_api(app):
    """
    API functions

    """
    app.route('/api/tasks/all')(api_tasks_get)
    app.route('/api/tasks/find', methods=['PUT'])(api_tasks_find)
    app.route('/api/tasks/add', methods=['PUT'])(api_tasks_add_put)
    app.route('/api/tasks/remove', methods=['PUT'])(api_remove_tasks)
    # app.route('/api/tasks/export', methods=['PUT'])(api_tasks_export)
    # app.route('/api/tasks/import', methods=['PUT'])(api_tasks_import)


# @login_required
def api_tasks_get():
    q = {}
    cursor = mongo_db.tasks.find(q)
    rez = []
    for el in cursor:
        el['_id'] = str(el['_id'])
        rez.append(el)
    return Response(json.dumps({'rez':rez}), mimetype='application/json')

def api_tasks_find():
    obj = request.json
    query = obj['query']
    sort = obj['sort']  # ('_id', -1)
    limit = obj['limit']
    skip = obj['skip']
    cursor = mongo_db.tasks.find(query).sort([sort]).skip(skip).limit(limit)
    total = cursor.count()
    rez = []
    for el in cursor:
        el['_id'] = str(el['_id'])
        rez.append(el)
    return Response(json.dumps({'res': rez, 'total': total}), mimetype='application/json')


def api_tasks_add_put():
    obj = request.json
    if 'selected' in obj:
        del obj['selected']
    if '_id' in obj:
        q = {'_id': ObjectId(obj['_id'])}
        obj['_id'] = ObjectId(obj['_id'])
        mongo_db.tasks.update(q, obj)
        obj = convert_dict_to_JSON(obj)
    else:
        id = mongo_db.tasks.insert(obj)
        obj['_id'] = str(id)
    return Response(json.dumps({'rez': obj}), mimetype='application/json')


def api_remove_tasks():
    feature = request.json
    q = {'_id': ObjectId(feature['_id'])}
    feature['__id'] = feature['_id']
    del feature['_id']
    mongo_db.tasks.remove(q)
    return Response(json.dumps({'res': 'ok'}), mimetype='application/json')

