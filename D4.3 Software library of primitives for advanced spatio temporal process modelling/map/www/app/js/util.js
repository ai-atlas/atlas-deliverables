
console.log('util.js se ucitva')
var wgs84Sphere = new ol.Sphere(6378137);

var formatLength = function(line) {
    // var length = ol.Sphere.getLength(line);
    var coordinates = line.getCoordinates();
    var length = 0;
    for (var i = 0, ii = coordinates.length - 1; i < ii; ++i) {
        var c1 = ol.proj.transform(coordinates[i], 'EPSG:3857', 'EPSG:4326');
        var c2 = ol.proj.transform(coordinates[i + 1], 'EPSG:3857', 'EPSG:4326');
        length += wgs84Sphere.haversineDistance(c1, c2);
    }

    var output;
    if (length > 100) {
        output = (Math.round(length / 1000 * 100) / 100) +
            ' ' + 'km';
    } else if (length > 1) {
        output = (Math.round(length * 100) / 100) +
            ' ' + 'm';
    } else {
        output = (Math.round(length *100* 100) / 100) +
            ' ' + 'cm';
    }
    return output;
};

var formatAreaText = function (area) {
    var output;
    if (area > 1000) {
        output = (Math.round(area / 10000 * 100) / 100) +
            ' ' + 'ha';
    } else {
        output = (Math.round(area * 100) / 100) +
            ' ' + 'm2'; //<sup>2</sup>
    }
    return output;
};



var formatArea = function (polygon) {
    var area;
    // if (geodesicCheckbox.checked) {
        var geom = polygon.clone().transform('EPSG:3857', 'EPSG:4326');
        var coordinates = geom.getLinearRing(0).getCoordinates();
        area = Math.abs(wgs84Sphere.geodesicArea(coordinates));
    // } else {
    //     area = polygon.getArea();
    // }
    return formatAreaText(area);
};

// var formatArea = function (polygon) {
//     var area = ol.Sphere.getArea(polygon);
//     return formatAreaText(area);
// };

var styles = {
    'Point': [new ol.style.Style({
            image: new ol.style.Circle({
                radius: 3,
                fill: new ol.style.Fill({
                    color: 'black'
                })
            })
        })],
    'LineString': [new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'green',
                width: 2
            })
        })],
    'Box': [new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'green',
                width: 1
            }),
            text: new ol.style.Text({
              text: 'Hello',
              scale: 1.3,
              fill: new ol.style.Fill({
                color: '#000000'
              })
            })                          
        })],
    'Polygon': [new ol.style.Style({
      stroke: new ol.style.Stroke({
          color: 'green',
          width: 3
      }),
      text: new ol.style.Text({
        text: 'Hello',
        scale: 1.3,
        fill: new ol.style.Fill({
          color: '#000000'
        })
      })                          
    })],
  'Circle': [new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'red',
                width: 2
            }),
            fill: new ol.style.Fill({
                color: 'rgba(255,0,0,0.2)'
            })
        })]
};
var styleFunction = function (feature, resolution) {
    if (feature.getGeometryName() === 'test') {
        return styleEnd;
    } else {
        return styles[feature.getGeometry().getType()];
    }
};


var arrowFunction = function (feature, resolution) {
    var obj = feature.get('obj');
    if (resolution > 0.3) {
        var arrowStyle = new ol.style.Style({});
        return arrowStyle;
    }else{
        var arrowStyle = new ol.style.Style({
            geometry: new ol.geom.Point(obj.pola),
            image: new ol.style.Icon({
                src: 'images/arrow.png',
                anchor: [0.75, 0.5],
                rotateWithView: true,
                rotation: -obj.rotation
            })
        });
        return arrowStyle;
    }
};

var tractorFunction = function (feature, resolution) {
    var obj = feature.get('obj');
    // if (resolution > 1) {
    //     var arrowStyle = new ol.style.Style({});
    //     return arrowStyle;
    // } else {
        var arrowStyle = new ol.style.Style({
            geometry: new ol.geom.Point(obj.pola),
            image: new ol.style.Icon({
                src: 'images/tractor_top_32.png',
                anchor: [0.75, 0.5],
                rotateWithView: true,
                rotation: -obj.rotation
            })
        });
        return arrowStyle;
    // }
};

var createPointStyle = function(color, radius, width){
    var ret = function(feature, resolution){
        var obj = feature.get('obj');
        var zIndex = 0;
        if(obj && obj.zIndex){
            zIndex = obj.zIndex;
        }
        var r = 5;
        if(radius != undefined){
            r = radius;
        }
        var style = new ol.style.Style({
            image: new ol.style.Circle({
                radius: r,
                // fill: new ol.style.Fill({
                //     color: "#777"
                // }),
                stroke: new ol.style.Stroke({ color: color, width: width })
            })
        })
        return style;
    };
    return ret;
}


var createLabelStyle = function (label, color) {
    return new ol.style.Text({
        textAlign: 'center',
        textBaseline: 'middle',
        font: 'normal 13px Arial',
        text:  label,
        fill: new ol.style.Fill({ color: color }),
        stroke: new ol.style.Stroke({ color: color, width: 2}),
        placement: 'point',
        rotation: 0
    });
};

var createLineStyle = function(color, width){
    var ret = function(feature, resolution){
        var obj = feature.get('obj');
        var label = '';
        if(obj.label!=undefined){
            label = obj.label;
        }
        var style = new ol.style.Style({
            stroke: new ol.style.Stroke({ color: color, width: width }),
            // text: new ol.style.Text({
            //     textAlign: 'center',
            //     textBaseline: 'middle',
            //     font: 'normal 13px Arial',
            //     text: label,//getText223(feature, resolution),
            //     fill: new ol.style.Fill({ color: "#000" }),
            //     // stroke: new ol.style.Stroke({ color: "#000", width: 1}),
            //     offsetX: 10,
            //     offsetY: 10,
            //     placement: 'point',
            //     maxAngle: 0.7853981633974483,
            //     overflow: false,
            //     rotation: 0
            // })
        })
        return style;
    };
    return ret;
}


var defaultStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: "#0ff",
        width: 8
    }),
    zIndex: 0
});



var getText223 = function (feature, resolution) {
    var maxResolution = 10;
    var obj = feature.get('obj');
    var text = "";//formatAreaText(obj.size);
    if (obj.title)
        text = obj.title + ' ' + formatAreaText(obj.size);
    if (obj.desc)
        text = obj.naziv + ' ' + formatAreaText(obj.area);
    if (obj.naziv)
        text = obj.naziv + ' ' + formatAreaText(obj.area);
    if (obj.crop)
        text = obj.crop.naziv + ' ' + formatAreaText(obj.area);
    if (obj.wo_id)
        text = 'RN:'+obj.wo_id + ' ' + formatAreaText(obj.area);

    if (resolution > maxResolution) {
        text = '';
    }
    return text;
};


var createTextStyle = function (feature, resolution) {
    var obj = feature.get('obj');
    var offsetY = 10;
    var str = '';
    if(obj.features){
        if(obj.features.points){
            str = ""+obj.features.points;
        }else{
            str = ""+obj.features.length;
        }
    }

    return new ol.style.Text({
        textAlign: 'center',
        textBaseline: 'middle',
        font: 'normal 13px Arial',
        text:  str,//getText223(feature, resolution),
        fill: new ol.style.Fill({ color: "#000" }),
        // stroke: new ol.style.Stroke({ color: "#000", width: 1}),
        offsetX: 10,
        offsetY: offsetY,
        placement: 'point',
        maxAngle: 0.7853981633974483,
        overflow: false,
        rotation: 0
    });
};


var roverStyle = function (feature, resolution) {
    var colors = { '1': '#ff0000', '2': '#aa0000', '3': '#ff00ff', '4': '#0000ff', '5': '#00aa00' }
    var obj = feature.get('obj');
    var name = feature.get('name');
    var cc = '#000';
    if(obj.type){

        if (obj.type == 'parcels') {
            return new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 3,
                    fill: new ol.style.Fill({ color: '#0ff' }),
                    stroke: new ol.style.Stroke({ color: '#f0f', width: 1 })
                }),
            });
        }



        return new ol.style.Style({
            image: new ol.style.Circle({
                radius: 3,
                fill: new ol.style.Fill({ color: '#0ff' }),
                stroke: new ol.style.Stroke({ color: '#f0f', width: 1 })
            }),
        });
    }
    if (obj.gnss) {
        cc = colors[obj.gnss.fix]
    }
    try {
        var vacc = parseFloat(obj.gnss.vacc)
        var hacc = parseFloat(obj.gnss.hacc)
        if (vacc < 0.03 && hacc < 0.03) {
            return new ol.style.Style({
                // stroke: new ol.style.Stroke({
                //     color: el.stroke.color,
                //     width: el.stroke.width
                // }),
                image: new ol.style.Circle({
                    radius: 3,
                    fill: new ol.style.Fill({ color: '#ff0' }),
                    stroke: new ol.style.Stroke({ color: cc, width: 1 })
                }),
                // fill: new ol.style.Fill({
                //     color: 'rgba(0, 255, 255, 0.1)'
                // })
            });
        }
    } catch (err) {

    }
    if(name == 'pozicija'){
        return new ol.style.Style({
            image: new ol.style.Circle({
                radius: 5,
                fill: new ol.style.Fill({ color: '#000' }),
                stroke: new ol.style.Stroke({ color: '#ff0', width: 2 })
            })
        });
    }

    return new ol.style.Style({
        image: new ol.style.Circle({
            radius: 3,
            fill: new ol.style.Fill({ color: '#555' }),
            stroke: new ol.style.Stroke({ color: cc, width: 1 })
        })
    });
}


var formatBigDecimal = function(value){
    var output = value;
    if (value > 1000000) {
        output = (Math.round(value / (1000 * 100))/10) +' ' + 'M';
    } else if (value > 1000) {
        output = (Math.round(value/ 1000)) +'' + 'k';
    }
    return output; 
}

var formatDecimal = function(value){
    var output = value;
    output = Math.round(value*100)/100
    return output; 
}

var modifyPolygonStyle = function(feature, resolution) {
    var color = "#aa0000";
    var fillColor = 'rgba(0, 255, 255, 0.1)';
    var width = 3;
    var obj = feature.get('obj');
    return new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: color,
            width: width
        }),
        fill: new ol.style.Fill({
            color: fillColor
        }),
        zIndex:1001,
        text: createTextStyle(feature, resolution)
    });
}

var createPolygonStyle = function(strokeColor, strokeWidth, fillColor) {
    var color = strokeColor;
    var fillColor = fillColor;
    var width = strokeWidth;
    var ret = function(feature, resolution) {
        var obj = feature.get('obj');
        return new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: color,
                width: width
            }),
            fill: new ol.style.Fill({
                color: fillColor
            }),
            zIndex:1001,
            text: createTextStyle(feature, resolution)
        });
    };
    return ret;
}


var createPointStyle_old = function(color){
    var ret = function(feature, resolution){
        var obj = feature.get('obj');
        var zIndex = 0;
        if(obj && obj.zIndex){
            zIndex = obj.zIndex;
        }

        var style = new ol.style.Style({
            image: new ol.style.Circle({
                radius: 5,
                // fill: new ol.style.Fill({
                //     color: "#777"
                // }),
                stroke: new ol.style.Stroke({ color: color, width: 1 })
            })
        })
        return style;
    };
    return ret;
}


var print = function(el){
    console.log(el);
}

// Array.prototype.last = function () {
//     return this[this.length - 1];
// }

var inside = function(point, vs) {
    // ray-casting algorithm based on
    // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

    var x = point[0], y = point[1];

    var inside = false;
    for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
        var xi = vs[i][0], yi = vs[i][1];
        var xj = vs[j][0], yj = vs[j][1];

        var intersect = ((yi > y) != (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }

    return inside;
};


var Point = function(x, y, z){
    var vm = this;
    vm.x = x;
    vm.y = y;
    vm.z = z;
    vm.xyz = [vm.x, vm.y, vm.z];

    vm.translate = function(point){
        vm.x += point.x;
        vm.y += point.y;
        vm.z += point.z;
        vm.xyz = [vm.x, vm.y, vm.z];
    }

    vm.rotate = function(RM){
        var C = [0, 0, 0];
        for(var i=0; i<3; i++){
            for(var j=0; j<3; j++){
                C[i] += RM[i][j]*xyz[j];
            }
        }
        vm.x = C[0];
        vm.y = C[1];
        vm.z = C[2];
        vm.xyz = [vm.x, vm.y, vm.z];
        return C;
    }

    vm.toList = function(){
        return [vm.x, vm.y, vm.z];
    }
}

// helper classes 
var Vector = function (x, y) {
    this.x = x;
    this.y = y;
    vm = this;
};

var simplifyPath = function (points, tolerance) {

    var Line = function (p1, p2) {
        this.p1 = p1;
        this.p2 = p2;

        this.distanceToPoint = function (point) {
            // slope
            var m = (this.p2.y - this.p1.y) / (this.p2.x - this.p1.x),
                // y offset
                b = this.p1.y - (m * this.p1.x),
                d = [];
            // distance to the linear equation
            d.push(Math.abs(point.y - (m * point.x) - b) / Math.sqrt(Math.pow(m, 2) + 1));
            // distance to p1
            d.push(Math.sqrt(Math.pow((point.x - this.p1.x), 2) + Math.pow((point.y - this.p1.y), 2)));
            // distance to p2
            d.push(Math.sqrt(Math.pow((point.x - this.p2.x), 2) + Math.pow((point.y - this.p2.y), 2)));
            // return the smallest distance
            return d.sort(function (a, b) {
                return (a - b); //causes an array to be sorted numerically and ascending
            })[0];
        };
    };

    var douglasPeucker = function (points, tolerance) {
        if (points.length <= 2) {
            return [points[0]];
        }
        var returnPoints = [],
            // make line from start to end 
            line = new Line(points[0], points[points.length - 1]),
            // find the largest distance from intermediate poitns to this line
            maxDistance = 0,
            maxDistanceIndex = 0,
            p;
        for (var i = 1; i <= points.length - 2; i++) {
            var distance = line.distanceToPoint(points[i]);
            if (distance > maxDistance) {
                maxDistance = distance;
                maxDistanceIndex = i;
            }
        }
        // check if the max distance is greater than our tollerance allows 
        if (maxDistance >= tolerance) {
            p = points[maxDistanceIndex];
            line.distanceToPoint(p, true);
            // include this point in the output 
            returnPoints = returnPoints.concat(douglasPeucker(points.slice(0, maxDistanceIndex + 1), tolerance));
            // returnPoints.push( points[maxDistanceIndex] );
            returnPoints = returnPoints.concat(douglasPeucker(points.slice(maxDistanceIndex, points.length), tolerance));
        } else {
            // ditching this point
            p = points[maxDistanceIndex];
            line.distanceToPoint(p, true);
            returnPoints = [points[0]];
        }
        return returnPoints;
    };
    var arr = douglasPeucker(points, tolerance);
    // always have to push the very last point on so it doesn't get left off
    arr.push(points[points.length - 1]);
    return arr;
};

function parse_query_string(query) {
    var vars = query.split("&");
    var query_string = {};
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      var key = decodeURIComponent(pair[0]);
      var value = decodeURIComponent(pair[1]);
      // If first entry with this name
      if (typeof query_string[key] === "undefined") {
        query_string[key] = decodeURIComponent(value);
        // If second entry with this name
      } else if (typeof query_string[key] === "string") {
        var arr = [query_string[key], decodeURIComponent(value)];
        query_string[key] = arr;
        // If third or later entry with this name
      } else {
        query_string[key].push(decodeURIComponent(value));
      }
    }
    return query_string;
  }




  function http_get(url, callback){
    var xhr = new XMLHttpRequest();
    xhr.onload = function(e) {
        var buf = xhr.response; // not responseText
        var ndarray = fromArrayBuffer(buf);
        callback(ndarray);
    };
    xhr.open("GET", url, true);
    xhr.responseType = "arraybuffer";
    xhr.send(null);
  }

  function http_get_simple(url, callback){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onload = function(e) {
        callback(JSON.parse(xhr.response));
    };
    xhr.send(null);
  }


  function http_put(url, data, callback){
    var json = JSON.stringify(data);
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", url, true);
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhr.onload = function () {
        callback(JSON.parse(xhr.response));
    }
    xhr.send(json);      
  }
