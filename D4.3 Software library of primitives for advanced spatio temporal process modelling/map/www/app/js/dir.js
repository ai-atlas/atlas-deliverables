(function () {
  'use strict';
  var app = angular.module('myApp');

  // keyboard events
  var KEY_DW = 40;
  var KEY_RT = 39;
  var KEY_UP = 38;
  var KEY_LF = 37;
  var KEY_ES = 27;
  var KEY_EN = 13;
  var KEY_TAB = 9;


  app.directive('draggable', ['$document', function ($document) {
    return {
      restrict: 'A',
      link: function (scope, elm, attrs) {
        var startX, startY, initialMouseX, initialMouseY;
        elm.css({ 'z-index': 1000, 'position': 'absolute' });

        elm.bind('mousedown', function ($event) {
          startX = elm.prop('offsetLeft');
          startY = elm.prop('offsetTop');
          initialMouseX = $event.clientX;
          initialMouseY = $event.clientY;
          $document.bind('mousemove', mousemove);
          $document.bind('mouseup', mouseup);
          return false;
        });

        function mousemove($event) {
          var dx = $event.clientX - initialMouseX;
          var dy = $event.clientY - initialMouseY;
          //dy = 0;
          elm.css({
            top: startY + dy + 'px',
            left: startX + dx + 'px'
          });
          return false;
        }

        function mouseup() {
          $document.unbind('mousemove', mousemove);
          $document.unbind('mouseup', mouseup);
        }
      }
    };
  }]);


  app.directive("sort", function () {
    return {
      restrict: 'A',
      transclude: true,
      template:
        '<a ng-click="onClick()">' +
        '<span ng-transclude></span>' +
        '<i class="fa" ng-class="{\'fa-sort-down\' : order === by && !reverse,  \'fa-sort-up\' : order===by && reverse}"></i>' +
        '</a>',
      scope: {
        order: '=',
        by: '=',
        reverse: '='
      },
      link: function (scope, element, attrs) {
        scope.onClick = function () {
          if (scope.order === scope.by) {
            scope.reverse = !scope.reverse
          } else {
            scope.by = scope.order;
            scope.reverse = false;
          }
        }
      }
    }
  });

  app.directive('keyFocus', function () {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        var atomSelector = attrs.keyFocus;

        elem.bind('keydown keypress', function (e) {
          var atoms = angular.element(document.querySelectorAll('[key-focus]'));
          var toAtom = null;
          var matrica = {};

          if (e.target.attributes.kolona) {
            var kolona2 = parseInt(e.target.attributes.kolona.nodeValue);
            var vrsta2 = parseInt(e.target.attributes.vrsta.nodeValue);
            if (e.target.attributes.grupa) {
              var grupa2 = e.target.attributes.grupa.nodeValue;
              if (grupa2) {
                var lista = [];
                for (var i = 0; i < atoms.length; i++) {
                  if (atoms[i].attributes.grupa && atoms[i].attributes.grupa.nodeValue == grupa2) {
                    lista.push(atoms[i])
                  }
                }
                atoms = lista;
              }
            }
          }
          // proveriti samo one koji su vidljivi
          // var lista = [];
          // for (var i = 0; i < atoms.length; i++) {
          //   if (atoms[i].is(':visible')) {
          //     lista.push(atoms[i])
          //   }
          // }
          // atoms = lista;

          var navigacija = false;

          var val = e.target.value;
          if (e.target.tagName != 'SELECT' && val) {
            var pozicija = val.slice(0, e.target.selectionStart).length;

            if (e.keyCode === 38) {
              navigacija = true;
            } else if (e.keyCode === KEY_DW) {
              navigacija = true;
            } else if (e.keyCode === 37) { // levo
              if (pozicija == 0)
                navigacija = true;
            } else if (e.keyCode === 39) { // desno
              if (pozicija == val.length)
                navigacija = true;
            }
          } else {
            navigacija = true;
          }

          if (navigacija) {
            for (var i = atoms.length - 1; i >= 0; i--) {
              try {
                var kolona = parseInt(atoms[i].attributes.kolona.nodeValue);
                var vrsta = parseInt(atoms[i].attributes.vrsta.nodeValue);
                if (!matrica[vrsta]) {
                  matrica[vrsta] = {}
                }
                matrica[vrsta][kolona] = atoms[i]
              } catch (exp) {

              }
            }
            try {
              if (e.keyCode === 38) {
                toAtom = matrica[vrsta2 - 1][kolona2];
                e.preventDefault();
              } else if (e.keyCode === KEY_DW) {
                toAtom = matrica[vrsta2 + 1][kolona2];
                e.preventDefault();
              } else if (e.keyCode === 37) { // levo
                toAtom = matrica[vrsta2][kolona2 - 1];
                e.preventDefault();
              } else if (e.keyCode === 39) { // desno
                toAtom = matrica[vrsta2][kolona2 + 1];
                e.preventDefault();
              }
            } catch (exp) {

            }
            //console.log(vrsta2, kolona2)
          }

          // for (var i = atoms.length - 1; i >= 0; i--) {
          //     var kolona = e.target.attributes.kolona.nodeValue;
          //     var vrsta = e.target.attributes.vrsta.nodeValue;
          //     if (atoms[i] === e.target) {
          //         if (e.keyCode === 38) {
          //             toAtom = atoms[i - 1];
          //             e.preventDefault();
          //         } else if (e.keyCode === 40) {
          //             toAtom = atoms[i + 1];
          //             e.preventDefault();
          //         }
          //         break;
          //     }
          // }
          if (toAtom) toAtom.focus();
        });

        // elem.bind('keydown', function (e) {
        //       if (e.keyCode === 38 || e.keyCode === 40)
        //           e.preventDefault();
        //   });
        /*
         elem.on('keydown', atomSelector, function (e) {
              if (e.keyCode === 38 || e.keyCode === 40)
                  e.preventDefault();
          });
          */
      }
    };
  });


  app.directive('myEnter', function () {
    return function (scope, element, attrs) {
      element.bind("keydown keypress", function (event) {
        if (event.which === 13) {
          scope.$apply(function () {
            scope.$eval(attrs.myEnter);
          });
          event.preventDefault();
        }
      });
    };
  });

  app.directive('myTopnav', function () {
    return function (scope, element, attrs) {
      element.bind("click", function (event) {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
          x.className += " responsive";
        } else {
          x.className = "topnav";
        }

      });
    };
  });


  app.directive('focusMe', function ($timeout) {
    return {
      restrict: 'A',
      scope: {
        focusMe: '='
      },
      link: function (scope, elt) {
        scope.$watch('focusMe', function (val) {
          if (val) {
            elt[0].focus();
          }
        });
      }
    };
  });

  // app.directive('formField', function () {
  //   return {
  //     restrict: 'E',
  //     replace: 'true',
  //     scope: true,
  //     require: 'ngModel',
  //     template:
  //       '<tr class="tr-narrow">'+
  //       '  <td ng-class="ngClass.$viewValue">' +
  //       '    {{ label }}' +
  //       '  </td>' +
  //       '  <td ng-class="ngClass.$viewValue">' +
  //       '    <input type="text" ng-model="ngModel.$viewValue" ' +
  //       '        size=20 class="table_input"' +
  //       '        ng-focus="ngFocus.$viewValue"' +
  //       '        ng-blur="ngBlur.$viewValue"' +
  //       '        key-focus vrsta="{{vrsta}}" kolona="{{kolona}}">' +
  //       '  </td>' +
  //       '</tr>',
  //     link: function (scope, element, attributes, ngModel) {
  //       scope.label = attributes.label;
  //       scope.ngClass = ngClass;
  //       scope.ngModel = ngModel;
  //       scope.ngFocus = ngFocus;
  //       scope.ngBlur = ngBlur;
  //       scope.kolona = attributes.kolona;
  //       scope.vrsta = attributes.vrsta;

  //       scope.$watch(function () {
  //         return ngModel.$viewValue;
  //       }, function (newValue) {
  //         ngModel.$setViewValue(newValue);
  //         ngModel.$render();
  //       });
  //     }
  //   };
  // });
  // <form-field
  //   label="Broj naloga"
  //   ng-class="rn_ctrl.rnDijalog.wo_id.focused?'row_focused':''"
  //   ng-model="rn_ctrl.wo.test"
  //   ng-focus="rn_ctrl.rnDijalog.wo_id.focused = true"
  //   ng-blur="rn_ctrl.rnDijalog.wo_id.focused=false;"
  //   vrsta="1" kolona="1">



  app.directive('myField', function () {
    return {
      restrict: 'E',
      replace: 'true',
      scope: true,
      require: 'ngModel',
      template:
        '<div class="col-md-6 form-group field">' +
        '<input id="{{ labelId }}" class="form-control" placeholder="{{ label }}" ng-model="ngModel.$viewValue">' +
        '<label for="{{ labelId }}">{{ label }}</label>' +
        '</div>',
      link: function (scope, element, attributes, ngModel) {
        scope.label = attributes.label;
        scope.labelId = attributes.labelId;
        scope.type = attributes.type;
        scope.required = attributes.required;

        scope.ngModel = ngModel;
        scope.$watch(function () {
          return ngModel.$viewValue;
        }, function (newValue) {
          ngModel.$setViewValue(newValue);
          ngModel.$render();
        });
      }
    };
  });

  // app.directive('mySelect', function() {
  //   return {
  //     restrict: 'E',
  //     replace: 'true',
  //     scope: true,
  //     require: 'ngModel',
  //     template:
  //     '<div class="col-md-6 form-group field">'+
  //       '<input id="{{ labelId }}" class="form-control" placeholder="{{ label }}" ng-model="ngModel.$viewValue">'+
  //       '<label for="{{ labelId }}">{{ label }}</label>'+
  //     '</div>',
  //     link: function(scope, element, attributes, ngModel) {
  //       scope.label = attributes.label;
  //       scope.labelId = attributes.labelId;
  //       scope.type = attributes.type;
  //       scope.required = attributes.required;

  //       scope.ngModel = ngModel;
  //       scope.$watch(function() {
  //         return ngModel.$viewValue;
  //       }, function(newValue) {
  //         ngModel.$setViewValue(newValue);
  //         ngModel.$render();
  //       });
  //     }
  //   };
  // });

  app.directive('copyToClipboard', function ($q) {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        elem.click(function () {
          if (attrs.copyToClipboard) {
            var $temp_input = angular.element("<input>");
            angular.element("body").append($temp_input);
            $temp_input.val(attrs.copyToClipboard).select();
            document.execCommand("copy");
            $temp_input.remove();
          }
        });
      }
    };
  });



  /********************** */
  app.directive('autoComplete', ['$q', '$http', '$timeout', '$interpolate',
    function ($q, $http, $timeout, $interpolate) {

      // string constants
      var REQUIRED_CLASS = 'autocomplete-required';
      var TEXT_SEARCHING = 'Searching...';
      var TEXT_NORESULTS = 'No results found';
      var TEMPLATE_URL = '/angucomplete-alt/index.html';

      // Set the default template for this directive
      var template =
        '<div class="auto-complete-holder">' +
        '  <input id="{{id}}_value" class="table_input" name="{{inputName}}" ng-model="searchStr" type="text" ' +
        '     placeholder="{{placeholder}}" kolona="{{kolona}}" vrsta="{{vrsta}}"/>' +
        '     <div ng-style="style" ng-click="toggleResults()"><i class="fas fa-chevron-down"></i></div>' +
        '  <div id="{{id}}_dropdown" class="auto-complete-dropdown" ng-show="showDropdown">' +
        '    <div class="auto-complete-searching" ng-show="searching" ng-bind="textSearching"></div>' +
        '    <div class="auto-complete-searching" ng-show="!searching && (!results || results.length == 0)" ng-bind="textNoResults"></div>' +
        '    <div class="auto-complete-row" ng-repeat="result in results|filter: searchF" ng-click="selectResult(result)"' +
        '         ng-mouseenter="hoverRow($index)" ' +
        '         ng-class="{\'auto-complete-selected-row\': $index == currentIndex}">' +
        '        {{formatTitle(result)}}' +
        '    </div>' +
        '  </div>' +
        '</div>';

      function link(scope, elem, attrs, ctrl) {
        var inputField = elem.find('input');
        var dd = elem[0].querySelector('.auto-complete-dropdown');
        var isScrollOn = false;
        var mousedownOn = null;
        var unbindInitialValue;

        scope.transform = '';
        scope.style = {
          'position': 'absolute',
          'right': '0px',
          'top': '0px',
          'transform': scope.transform,
          '-webkit-transform': scope.transform,
          '-ms-transform': scope.transform,
          'transition': '0.5s'
        }

        scope.toggleResults = function () {
          if (scope.showDropdown == true) {
            scope.transform = "";
          } else {
            scope.transform = "rotate(180deg)";
          }
          scope.searchStr = "";
          if (scope.showDropdown == true) {
            scope.closeDropDown();
          } else {
            scope.openDropDown();
          }
          //scope.$apply();
        }


        elem.on('mousedown', function (event) {
          // if (event.target.id) {
          //   mousedownOn = event.target.id;
          //   if (mousedownOn === scope.id + '_dropdown') {
          //     document.body.addEventListener('click', clickoutHandlerForDropdown);
          //   }
          // }
          // else {
          //   mousedownOn = event.target.className;
          // }
        });


        scope.searchStr = '';
        scope.searchF = function (item) {
          var q = scope.searchStr.toLowerCase();
          var tt = scope.formatTitle(item);
          if (tt.toLowerCase().indexOf(q) != -1)
            return true;
          else
            return false;
        }

        function dropdownRowOffsetHeight(row) {
          var css = getComputedStyle(row);
          return row.offsetHeight +
            parseInt(css.marginTop, 10) + parseInt(css.marginBottom, 10);
        }

        function dropdownHeight() {
          return dd.getBoundingClientRect().top +
            parseInt(getComputedStyle(dd).maxHeight, 10);
        }

        function dropdownRow() {
          return elem[0].querySelectorAll('.auto-complete-row')[scope.currentIndex];
        }

        function dropdownRowTop() {
          return dropdownRow().getBoundingClientRect().top -
            (dd.getBoundingClientRect().top +
              parseInt(getComputedStyle(dd).paddingTop, 10));
        }

        function dropdownScrollTopTo(offset) {
          dd.scrollTop = dd.scrollTop + offset;
        }

        // for IE8 quirkiness about event.which
        function ie8EventNormalizer(event) {
          return event.which ? event.which : event.keyCode;
        }

        scope.currentIndex = scope.focusFirst ? 0 : null;
        scope.searching = false;
        function keydownHandler(event) {
          var which = ie8EventNormalizer(event);
          var row = null;
          var rowTop = null;

          console.log(which, scope.currentIndex);
          if (which === KEY_EN && scope.results) {
            if (scope.currentIndex >= 0 && scope.currentIndex < scope.results.length) {
              event.preventDefault();
              scope.selectResult(scope.results[scope.currentIndex]);
            }
            scope.$apply();
          } else if (which === KEY_DW && scope.results) {
            event.preventDefault();
            if ((scope.currentIndex + 1) < scope.results.length && scope.showDropdown) {
              scope.$apply(function () {
                scope.currentIndex++;
                console.log(scope.currentIndex);
              });

              if (isScrollOn) {
                row = dropdownRow();
                if (dropdownHeight() < row.getBoundingClientRect().bottom) {
                  dropdownScrollTopTo(dropdownRowOffsetHeight(row));
                }
              }
            }
          } else if (which === KEY_UP && scope.results) {
            event.preventDefault();
            if (scope.currentIndex >= 1) {
              scope.$apply(function () {
                scope.currentIndex--;
                //updateInputField();
                console.log(scope.currentIndex);
              });

              if (isScrollOn) {
                rowTop = dropdownRowTop();
                if (rowTop < 0) {
                  dropdownScrollTopTo(rowTop - 1);
                }
              }
            }
            else if (scope.currentIndex === 0) {
              scope.$apply(function () {
                scope.currentIndex = -1;
                // inputField.val(scope.searchStr);
              });
            }
          }

        }

        scope.selectResult = function (el) {
          console.log(el);
          scope.selectedObject = el;
          // scope.showDropdown = false;
          scope.toggleResults();
          //scope.results = null;
        }

        scope.openDropDown = function () {
          var lista = [];
          for (var i in scope.localData) {
            var el = scope.localData[i];
            lista.push(el);
          }
          scope.style = {
            'position': 'absolute',
            'right': '0px',
            'top': '0px',
            'transform': scope.transform,
            '-webkit-transform': scope.transform,
            '-ms-transform': scope.transform,
            'transition': '0.5s'
          }
          scope.results = lista;
          scope.currentIndex = -1;
          scope.showDropdown = true;

          // scope.$apply(function () {
          // })
        }

        scope.closeDropDown = function () {
          scope.transform = "";
          scope.style = {
            'position': 'absolute',
            'right': '0px',
            'top': '0px',
            'transform': scope.transform,
            '-webkit-transform': scope.transform,
            '-ms-transform': scope.transform,
            'transition': '0.5s'
          }
          scope.results = [];
          scope.currentIndex = -1;
          scope.showDropdown = false;
          // scope.$apply(function () {
          // })
        }


        function keyupHandler(event) {
          var minlength = 2;
          var which = ie8EventNormalizer(event);
          if (which === KEY_LF || which === KEY_RT) {
            return;
          }

          if (which === KEY_UP) {
            event.preventDefault();
          } else if (which === KEY_EN) {
            event.preventDefault();

            if (scope.results && scope.currentIndex >= 0 && scope.currentIndex < scope.results.length) {
              scope.selectResult(scope.results[scope.currentIndex]);
              scope.$apply();
            } else {
              scope.openDropDown();
              scope.$apply();
            }


          } else if (which === KEY_DW) {
            event.preventDefault();
            if (!scope.showDropdown && scope.searchStr && scope.searchStr.length >= minlength) {
              //initResults();
              scope.searching = true;
              //searchTimerComplete(scope.searchStr);
            }
          }
          else if (which === KEY_ES) {
            scope.closeDropDown();
            scope.$apply();
            // scope.$apply(function () {
            //   scope.results = null;
            //   scope.showDropdown = false;
            // })
            // //clearResults();
            // scope.$apply(function () {
            //   inputField.val(scope.searchStr);
            // });
          }
        }

        scope.$watch('selectedObject', function (newval, oldval) {
          if (newval)
            scope.searchStr = scope.formatTitle(newval);
          // if (newval !== oldval) {
          //   if (!newval) {
          //   }
          // }
        });


        // register events
        inputField.on('keydown', keydownHandler);
        inputField.on('keyup compositionend', keyupHandler);
        inputField.on('focus', function () {
        });
        inputField.on('blur', function () {
          if (scope.results == null)
            scope.showDropdown = false;
        });


        // set isScrollOn
        $timeout(function () {
          var css = getComputedStyle(dd);
          isScrollOn = css.maxHeight && css.overflowY === 'auto';
        });

        scope.formatTitle = function (el) {
          return scope.title(el);
        }


      }

      return {
        restrict: 'EA',
        scope: {
          selectedObject: '=',
          title: '=',
          selectedObjectData: '=',
          localData: '=',
          localSearch: '&',
          id: '@',
          kolona: '@',
          vrsta: '@',
          placeholder: '@',
          textSearching: '@',
          textNoResults: '@',
          inputClass: '@',
          pause: '@',
          searchFields: '@',
          clearSelected: '@',
          overrideSuggestions: '@',
          fieldRequired: '=',
          fieldRequiredClass: '@',
          inputChanged: '=',
          autoMatch: '@',
          focusOut: '&',
          focusIn: '&',
          inputName: '@',
          parseInput: '&'
        },
        template: template,
        compile: function (tElement) {
          var startSym = $interpolate.startSymbol();
          var endSym = $interpolate.endSymbol();
          if (!(startSym === '{{' && endSym === '}}')) {
            var interpolatedHtml = tElement.html()
              .replace(/\{\{/g, startSym)
              .replace(/\}\}/g, endSym);
            tElement.html(interpolatedHtml);
          }
          return link;
        }
      };
    }]);

  /********************** */
})();

