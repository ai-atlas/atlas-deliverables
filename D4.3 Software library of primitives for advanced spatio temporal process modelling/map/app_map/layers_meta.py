#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

from flask_restful import reqparse, abort, Resource
from flask import request
from flask import Response
from bson.objectid import ObjectId

import json
import os
import numpy as np

from app.auth.util import login_required

from app.db import mongo_db
from app.util import DBObject
import itertools
import json
from app.util import convert_dict_to_JSON
from bson.objectid import ObjectId


def layers_meta_api(app):
    """
    API functions

    """
    app.route('/api/map-light/layers_meta_all', methods=['GET'])(api_map_light_layers_meta)
    app.route('/api/map-light/layers_meta', methods=['PUT'])(api_map_light_layers_meta_put)

def api_map_light_layers_meta():
    box = request.json
    query = {}
    cursor = mongo_db.layers_meta.find(query)
    rez = []
    for el in cursor:
        el['_id'] = str(el['_id'])
        rez.append(el)
    return Response(json.dumps({'rez': rez}), mimetype='application/json')

def api_map_light_layers_meta_put():
    feature = request.json
    print('-------------------------------------------------')
    print(feature)
    if '_id' in feature:
        q = {'_id': ObjectId(feature['_id'])}
        del feature['_id']
        doc = {'$set':feature}
        mongo_db.layers_meta.update(q, feature)
        print({'layer': feature['name']}, {'$set': {'visible': feature['visible']}})
        mongo_db.layers.update({'layer':feature['name']}, {'$set':{'visible': feature['visible']}}, multi=True)
    else:
        mongo_db.layers_meta.insert(feature)
    return Response(json.dumps({'rez': 'ok'}), mimetype='application/json')
