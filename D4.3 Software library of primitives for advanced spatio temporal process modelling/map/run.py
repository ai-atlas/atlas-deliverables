#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask
from flask import request
from flask import url_for
from flask import Response
from flask import Flask, abort, send_file
from flask import send_from_directory
from flask import send_file
from flask import jsonify
import jwt
import sys

import os
from os import listdir
from os.path import isfile, join

import datetime
import time
import json
import io
from pymongo import MongoClient
from bson.objectid import ObjectId
import numpy as np

flask_app = Flask(__name__, static_folder='www')
os.environ["APP_DB"] = "localhost"
# os.environ["APP_DB"] = "db"
CACHE_ROOT = "/media/djordje/DATA01/cache/"


import app
import app.util as util
from app.auth.util import ADMIN_USERS
from app.users import users_api
from app.files import files_routes
from app.db import mongo_db
import app_map

security_key = 'Levo.desno.1234'

PROJECT = "poc_1.2"
SECTION = "20" 
camera_id = "0"


@flask_app.after_request
def after_request(response):
    global start_time
    e = datetime.datetime.now()
    diff = time.time() - start_time
    util.requests_log(request.url, diff*1000)
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


@flask_app.before_request
def before_request():
    global start_time
    util.requests_log(request.url, None)
    start_time = time.time()
    request.__setattr__('admin_user', None)
    token = request.headers.get('auth-token')
    if token is not None and token in ADMIN_USERS:
        user = ADMIN_USERS[token]
        request.__setattr__('admin_user', user)


users_api(flask_app)
# files_routes(flask_app)

app_map.layers.layers_api(flask_app)
app_map.layers_meta.layers_meta_api(flask_app)

@flask_app.route('/<path:path>')
def static_file(path):
    try:
        return flask_app.send_static_file(path)
    except:
        return "Error"


@flask_app.route('/')
def root():
    return flask_app.send_static_file('index.html')

if __name__ == "__main__":
    flask_app.run(host='0.0.0.0', port=8085, debug=True, use_reloader=True)
