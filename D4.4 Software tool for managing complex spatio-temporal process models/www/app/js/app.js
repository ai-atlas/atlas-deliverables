
// (function () {
//   'use strict';

var app = angular.module('myApp', ['ui.bootstrap']);
var jq = $.noConflict();

app.controller('MyCtrl', function ($rootScope, $scope, $window, $http, $interval, $document, $timeout, $location) {
  var vm = this;
  $rootScope.menu = vm;

  vm.screenWidth = $window.innerWidth;
  vm.screenHeight = $window.innerHeight;
  angular.element($window).on('resize', function () {
    $scope.$apply(function () {
      vm.screenWidth = $window.innerWidth;
      vm.screenHeight = $window.innerHeight;
    });
  });

  vm.showSearch = false;

  vm.openMenu = function () {
    jq('#leftMenu').css({ left: '0px' });
  }

  vm.closeMenu = function () {
    jq('#leftMenu').css({ left: '-600px' });
  }


  vm.layerNames = []
  vm.layers = {}
  vm.viewChanged = function (box) {
    var data = box
    var req = {
      method: "PUT",
      data: data,
      url: "/api/layers"
    }
    $http(req).then(
      function (resp) {
        var obj = {};
        var layerNames = [];
        for (var i in resp.data.rez) {
          var el = resp.data.rez[i];
          if (el.layer in obj) {
            obj[el.layer].shapes.push(el);
          } else {
            obj[el.layer] = { name: el.layer, shapes: [el] };
            layerNames.push(el.layer);
          }
        }
        vm.layerNames = layerNames;
        vm.layers = obj;
        console.log(vm.layers);
      }, function (resp) {
        vm.message = 'error';
      });
  }


  vm.map = null;

  vm.popup_module = 'objects';

  vm.close = function () {
    vm.popup_module = null;
  }

  vm.showInfrastructure = false;

  vm.toggleInfrastructure = function () {
    vm.showInfrastructure = !vm.showInfrastructure;
  }

  vm.show = function (el) {
    vm.popup_module = el;

    vm.closeMenu();
    vm.showSearch = false;
    $window.localStorage.setItem('map-popup_module', vm.popup_module);

    $location.hash(el);
  }

  vm.lineAdded = function (el) {
    console.log(el);
    //vm.segments.push({type:'LineString', points: el});
  }

  vm.boxAdded = function (el) {
    console.log(el);
    //vm.segments.push({type:'Box', points: el});
  }

  vm.circleAdded = function (el) {
    console.log(el);
    //vm.segments.push({type:'Circle', center: el[0], radius: el[1]});
  }

  vm.styleSave = function (el) {
    var req = {
      method: 'PUT',
      data: el,
      url: '/api/styles/add',
      headers: { 'auth-token': $rootScope.token }
    }
    $http(req).then(function (res) {
      el = res.data.rez;
      vm.listaHt[el['_id']].saved = true;
    }, function (res) { });
  }

  vm.stylesChanged = function (el) {
    el.changed = true;
  }

  vm.styleBlur = function (el) {
    if (el.changed == true) {
      delete el.changed;
      delete el.saved;
      delete el.focused;
      var _desc = el._desc;
      delete el._desc;
      vm.styleSave(el);
    }
  }

  vm.style = {
    width: "450px",
    height: "500px"
  }

  vm.h_full = false;
  vm.toggleStyle = function () {
    if (vm.style.width == "450px") {
      vm.style.width = "700px"
    } else {
      vm.style.width = "450px"
    }
  }

  vm.toggleHStyle = function () {
    var h = '300px'
    if (vm.h_full == false) {
      var hh = $window.innerHeight;
      h = (hh - 130) + 'px';
      vm.h_full = true;
    } else {
      vm.h_full = false;
    }
    vm.style['height'] = h;
  }

  vm.init = function () {
    $rootScope.menu = vm;
    var hash = $location.hash();
    if (hash) {
      vm.popup_module = hash;
      vm.closeMenu();
    } else {
      var el = $window.localStorage.getItem('map-popup_module');
      if (el) {
        vm.popup_module = el;
      }
    }
  };

  vm.init();


});

// })();