import pandas as pd
import numpy as np

import util
import aqi

# https://www.epa.gov/criteria-air-pollutants/naaqs-table
# https://www.airnow.gov/sites/default/files/2020-05/aqi-technical-assistance-document-sept2018.pdf

important =[
    "PRSS",
    "TPP6",
    "RH2M",
    "T02M",
    "TCLD",
    "U10M",
    "V10M",
    "TMPS",
    "PBLH",
    "irradiance",
    "88101 PM2.5 FRM/FEM Mass",  # 'Micrograms/cubic meter (LC)']
    "81102", # PM10  Micrograms/cubic meter (25 C)
    "44201 Ozone", # Parts per million
    "42101 CO", # Parts per million
    "42401 SO2", # Parts per billion
    "42602 NO2" # Parts per billion
]

site_ids = [
    "11-001-0043", #(49587, 24) (27981, 385)
    "13-089-0002", #(44817, 24) (21468, 385)
    "18-097-0078", #(44147, 24) (16774, 385)
    "22-033-0009", #(49886, 24) (6569, 385)
    "32-003-0540" #(29777, 24) (9665, 385)
]

# util.show_fuzzy_point()

for site_id in site_ids:
    df = pd.read_csv(f"../data/{site_id}_merged.csv")
    df.dt = pd.to_datetime(df.dt)
    print(site_id, df.shape)

    aqi.calculate_aqi(df, site_id)
    util.show_fuzzy_point()
    continue
    util.show_signal(df[:24*15], 'dt', f'irradiance', site_id)
    util.show_sample(df[:1], 'dt', f'irradiance', site_id)
    util.show_sample(df[:1], 'dt', f'81102', site_id)
    util.show_sample(df[:1], 'dt', '44201 Ozone', site_id)

    util.show_histogram(df, f'88101 PM2.5 FRM/FEM Mass', site_id)
    util.show_histogram(df, f'81102', site_id)
    util.show_histogram(df, f'44201 Ozone', site_id)
    util.show_histogram(df, f'42101 CO', site_id)
    util.show_histogram(df, f'42401 SO2', site_id)
    util.show_histogram(df, f'42602 NO2', site_id)