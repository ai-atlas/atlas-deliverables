import pandas as pd
import numpy as np
import os
import yaml

import matplotlib.pyplot as plt
import matplotlib as mpl
import datetime

fields = [
    "dt",
    "lat",
    "lon",
    "PRSS",
    "TPP6",
    "RH2M",
    "T02M",
    "TCLD",
    "U10M",
    "V10M",
    "TMPS",
    "PBLH",
    "irradiance",
    "88101 PM2.5 FRM/FEM Mass",
    "81102",
    "44201 Ozone",
    "42101 CO",
    "42401 SO2",
    "42602 NO2",
    "site_id",
    "elevation",
    "land_use",
    "location_setting",
    "year"
]

important =[
    "PRSS",
    "TPP6",
    "RH2M",
    "T02M",
    "TCLD",
    "U10M",
    "V10M",
    "TMPS",
    "PBLH",
    "irradiance",
    "88101 PM2.5 FRM/FEM Mass",
    "81102",
    "44201 Ozone",
    "42101 CO",
    "42401 SO2",
    "42602 NO2"
]

df_all = pd.read_csv("epa_aqi_dataset.csv")
df_all.dt = pd.to_datetime(df_all.dt)

sites = df_all.site_id.unique()
for site_id in sites:
    df = df_all[df_all.site_id == site_id].copy()

    dd = pd.DataFrame()
    dd['dt'] = df.dt
    for el in important:
        dd[el] = df[el]

    for pre in range(1, 24):
        H = datetime.timedelta(hours=pre)
        dt_pre = df.dt.shift(periods=pre)
        delta = dd.dt - dt_pre
        for el in important:
            dd[f'{el}_{pre}'] = df[el].shift(periods=pre)
        dd = dd[delta==H].copy()
    dd.to_csv(f'data/{site_id}_merged.csv')
    print(site_id, df.shape, dd.shape),

print(df.shape)