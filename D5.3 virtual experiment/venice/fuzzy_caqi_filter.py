import pandas as pd
import numpy as np
import os
import yaml
# import tensorflow as tf
# from tensorflow import keras
# from tensorflow.keras import layers

from sklearn.model_selection import train_test_split
# from sklearn.metrics import accuracy_score
from xgboost import XGBRegressor
# import shap
import matplotlib.pyplot as plt


MODEL = 'xgboost'
# MODEL = 'nn'

PREDICTORS = [
                "PRSS",
                "TPP6",
                "RH2M",
                "T02M",
                "TCLD",
                "irradiance",
                "elevation",
                "land_use",
                "location_setting"
            ]

AQI = [
    '88101', # PM2.5 particle pollution
    '81102', # PM10 mass
    '44201', # ozone
    '42101', # carbon monoxide
    '42401', # sulfor dioxide
    '42602', # nitrogen dioxide
]

TARGET = [
    "caqi"
]

epa_merged = "/home/atlas/data/atlas_data/epa/merged_2"
stanice = pd.read_csv("../epa/data/usa_btex_sites.csv")
df_stanice = stanice.set_index('id')
print('=========================')
for el in df_stanice:
    print(el)

def caqi_index(row):
    # 0 very_low
    # 1 low 
    # 2 medium
    # 3 high
    # 4 very_high
    no2 = row[0]
    # pm10 = row[1]
    pm25 = row[1]

    def no2_index(value):
        borders = [50, 100, 200, 400]
        ret = 0
        for ret in range(len(borders)):
            if value<borders[ret]:
                return ret
        return 4
    def pm10_index(value):
        borders = [25, 50, 90, 180]
        ret = 0
        for ret in range(len(borders)):
            if value<borders[ret]:
                return ret
        return 4
    def pm25_index(value):
        borders = [15, 30, 55, 110]
        ret = 0
        for ret in range(len(borders)):
            if value<borders[ret]:
                return ret
        return 4

    # return max([no2_index(no2), pm10_index(pm10), pm25_index(pm25)])
    return max([no2_index(no2), pm25_index(pm25)])


venice_input_files = []
for id in df_stanice.index:
    for year in range(2015, 2022):
        filename = f'{epa_merged}/{id}/{year}.csv'
        if os.path.exists(filename):
            df = pd.read_csv(filename)
            df['elevation'] = df_stanice.elevation
            df['land_use'] = df_stanice.land_use
            df['location_setting'] = df_stanice.location_setting

            n = 0
            for c in df.columns:
                for ce in AQI:
                    if ce in c:
                        n += 1
            if n == len(AQI):
                # df['caqi'] = df[['42602 NO2','88101 PM2.5 FRM/FEM Mass'] ].apply(caqi_index, axis=1)
                print(f"{id} {year} {df.shape} {filename}")
                venice_input_files.append(filename)
                # for el in df:
                #     print(el)
df = pd.DataFrame()
df['filename'] = venice_input_files
df.to_csv('venice_filenames.csv', index=False)


# datetime
# lat
# lon
# CAPE
# CFZR
# CICE
# CINH
# CPP6
# CRAI
# CSNO
# DSWF
# HCLD
# LCLD
# LHTF
# LIB4
# LISD
# MCLD
# MSLP
# PBLH
# PRSS
# RH2M
# SHGT
# SHTF
# SOLM
# T02M
# TCLD
# TMPS
# TPP6
# U10M
# UMOF
# V10M
# VMOF
# ws
# wd
# ws_e
# wd_e
# mofi
# mofd
# mofi_e
# mofd_e
# Unnamed: 0
# dt
# 43204 Propane
# 45218 m-Diethylbenzene
# 45225 1,2,3-Trimethylbenzene
# 43954 n-Undecane
# 43212 n-Butane
# 43216 trans-2-Butene
# 45219 p-Diethylbenzene
# 43217 cis-2-Butene
# 43244 2,2-Dimethylbutane
# 43202 Ethane
# 43243 Isoprene
# 45201 Benzene
# 43960 2-Methylheptane
# 45210 Isopropylbenzene
# 45109 m/p Xylene
# 43291 2,3-Dimethylpentane
# 43227 cis-2-Pentene
# 43238 n-Decane
# 45203 Ethylbenzene
# 43252 2,3,4-Trimethylpentane
# 45212 m-Ethyltoluene
# 43233 n-Octane
# 43235 n-Nonane
# 43221 Isopentane
# 45202 Toluene
# 43261 Methylcyclohexane
# 43248 Cyclohexane
# 43231 n-Hexane
# 43263 2-Methylhexane
# 42602 NO2
# 45204 o-Xylene
# 43250 2,2,4-Trimethylpentane
# 45208 1,2,4-Trimethylbenzene
# 43253 3-Methylheptane
# 43249 3-Methylhexane
# 43220 n-Pentane
# 45220 Styrene
# 43230 3-Methylpentane
# 43280 1-Butene
# 45207 1,3,5-Trimethylbenzene
# 43214 Isobutane
# 43226 trans-2-Pentene
# 44201 Ozone
# 43284 2,3-Dimethylbutane
# 43232 n-Heptane
# 43262 Methylcyclopentane
# 43285 2-Methylpentane
# 45211 o-Ethyltoluene
# 43206 Acetylene
# 43247 2,4-Dimethylpentane
# 43242 Cyclopentane
# 43205 Propylene
# 43203 Ethylene
# 43224 1-Pentene
# 45213 p-Ethyltoluene
# 45209 n-Propylbenzene
# irradiance