import pandas as pd
import numpy as np
import os
import yaml

epa_merged = "/home/atlas/data/atlas_data/epa/merged_2"
stanice = pd.read_csv("../epa/data/usa_btex_sites.csv")
df_stanice = stanice.set_index('id')

for el in df_stanice:
    print(el)

filenames = pd.read_csv('venice_filenames.csv')

AQI = [
    '88101 PM2.5 FRM/FEM Mass', # PM2.5 particle pollution
    '81102', # PM10 mass
    '44201 Ozone', # ozone
    '42101 CO', # carbon monoxide
    '42401 SO2', # sulfor dioxide
    '42602 NO2', # nitrogen dioxide
    # '45201 Benzene'
]

COMMON_ROWS = [  # https://www.ready.noaa.gov/gdas1.php
    "dt",
    "lat",
    "lon",
    "PRSS",
    "TPP6",
    "RH2M",
    "T02M",
    "TCLD",
    "U10M",
    "V10M",
    "TMPS",
    "PBLH",
    "irradiance",
    # "elevation",
    # "land_use",
    # "location_setting"
]

for a in AQI:
    COMMON_ROWS.append(a)

df_merged = pd.DataFrame()
num = 0
for i in filenames.index:
    filename = filenames.filename[i]
    df = pd.read_csv(filename)
    parts = filename.split('/')
    site_id = parts[7]
    year = int(parts[8].replace('.csv', ''))
    lat = df_stanice.lat[site_id]
    lon = df_stanice.lon[site_id]
    elevation = df_stanice.elevation[site_id]
    land_use = df_stanice.land_use[site_id]
    location_setting = df_stanice.location_setting[site_id]

    df_complete = df[COMMON_ROWS].dropna()

    df_complete['site_id'] = site_id
    df_complete['elevation'] = elevation
    df_complete['land_use'] = land_use
    df_complete['location_setting'] = location_setting
    df_complete['year'] = year
    df_complete.reset_index(drop=True)

    df_merged = pd.concat([df_merged, df_complete], axis=0)
    print(site_id, df.shape, df_complete.shape, (df.shape[0]-df_complete.shape[0]), df_merged.shape)
    # num += df_complete.shape[0]
#     for di in df_complete.index:
#         values = df[AQI].values[di]
#         # print(np.sum(values))
#         num += 1
    # for el in df:
    #     print(el)
    # exit()
print(num)
print(df_merged.shape)
df_merged.to_csv('epa_aqi_dataset.csv', index=False)